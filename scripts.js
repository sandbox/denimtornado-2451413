(function ($) {
	Drupal.denim_admin = Drupal.denim_admin || {};
	Drupal.behaviors.denim_admin = {};
	Drupal.behaviors.denim_admin.attach = function (context) {
		Drupal.denim_admin.taskBar();
		Drupal.settings.tableHeaderOffset = 'Drupal.denim_admin.tableHeaderOffset';
		$('#toolbar a.toggle').bind('click', Drupal.denim_admin.taskBar);
	};

	Drupal.denim_admin.taskBar = function () {
		var tasksHeight = $('#tasks').height();
		var toolbarHeight = $('#toolbar').height();
		$("#tasks").css('top', toolbarHeight + 'px');
		$("body").css('padding-top', tasksHeight + toolbarHeight + 'px');
	};

	Drupal.denim_admin.tableHeaderOffset = function () {
		return $('#tasks').height() + $('#toolbar').height();
	};
})(jQuery);
