<div class="span-two-third">
  <?php print drupal_render_children($form); ?>
</div>
<div class="span-one-third">
    <div id="new-actions"></div>
  <?php print drupal_render($sidebar); ?>
</div>
